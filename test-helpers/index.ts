import supertest from 'supertest'
import { DateTime } from 'luxon'

import User from 'App/Models/User'
import Teacher from 'App/Models/Teacher'
import Student from 'App/Models/Student'
import Admin from 'App/Models/Admin'

import { StudentFactory, TeacherFactory, AdminFactory } from 'Database/factories'

export const baseUrl = `http://${process.env.HOST}:${process.env.PORT}`

export async function loginAndGetToken(): Promise<string> {
  const user = await User.findBy('username', 'testing')

  if (!user) {
    await User.create({ username: 'testing', password: 'test', role: 'admin' })
  }

  const { body } = await supertest(baseUrl)
    .post('/login')
    .send({ username: 'testing', password: 'test' })

  return body.token
}

export interface LoginResult {
  token: string
  id: number
}

export async function loginAsStudent(): Promise<LoginResult> {
  let user = await User.findBy('username', 'asuser')
  if (!user) {
    user = await User.create({ username: 'asuser', password: 'test', role: 'student' })
  }
  const student = await Student.create({
    firstname: 'efazef',
    lastname: 'test',
    birthDate: DateTime.now(),
  })
  await student.related('user').associate(user)

  const { body } = await supertest(baseUrl)
    .post('/login')
    .send({ username: 'asuser', password: 'test' })

  return { token: body.token, id: student.id }
}

export async function loginAsTeacher(): Promise<LoginResult> {
  let user = await User.findBy('username', 'asteacher')
  if (!user) {
    user = await User.create({ username: 'asteacher', password: 'test', role: 'teacher' })
  }
  const teacher = await Teacher.create({ firstname: 'fazef', lastname: 'defaz' })
  await teacher.related('user').associate(user)

  const { body } = await supertest(baseUrl)
    .post('/login')
    .send({ username: 'asteacher', password: 'test' })

  return { token: body.token, id: teacher.id }
}

export async function logout(token: string): Promise<void> {
  await supertest(baseUrl).post('/logout').auth(token, { type: 'bearer' })
}

export const paginationKeys = ['data', 'total', 'limit', 'page', 'last_page']

export function createFakeStudent(): Promise<Student> {
  return StudentFactory.with('user', 1, (user) => user.apply('student')).create()
}

export function createFakeTeacher(): Promise<Teacher> {
  return TeacherFactory.with('user', 1, (user) => user.apply('teacher')).create()
}

export function createFakeAdmin(): Promise<Admin> {
  return AdminFactory.with('user', 1, (user) => user.apply('admin')).create()
}
