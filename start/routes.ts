/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'

Route.get('/health', async ({ response }) => {
  const report = await HealthCheck.getReport()
  return report.healthy ? response.ok(report) : response.badRequest(report)
})

Route.where('id', /^[0-9]+$/)

Route.post('/login', 'AuthController.login')
Route.get('/init', 'AdminsController.getInit')
Route.post('/init', 'AdminsController.postInit')

Route.group(() => {
  Route.post('/logout', 'AuthController.logout')

  // Admins
  Route.resource('admins', 'AdminsController').apiOnly()

  // Classes
  Route.resource('classes', 'ClassesController').apiOnly()

  Route.group(() => {
    Route.post('students/:id', 'ClassesController.attachStudent')
    Route.delete('students/:id', 'ClassesController.detachStudent')

    Route.post('teachers/:id', 'ClassesController.attachTeacher')
    Route.delete('teachers/:id', 'ClassesController.detachTeacher')
  })
    .prefix('classes/:class_id')
    .where('class_id', /^[0-9]+$/)

  // Classrooms
  Route.resource('classrooms', 'ClassroomsController').apiOnly()

  // Groups
  Route.resource('groups', 'GroupsController').apiOnly()

  Route.group(() => {
    Route.post('students/:id', 'GroupsController.attachStudent')
    Route.delete('students/:id', 'GroupsController.detachStudent')
  })
    .prefix('groups/:group_id')
    .where('group_id', /^[0-9]+$/)

  // Lessons
  Route.resource('lessons', 'LessonsController').apiOnly()

  Route.group(() => {
    Route.post('teachers/:id', 'LessonsController.attachTeacher')
    Route.delete('teachers/:id', 'LessonsController.detachTeacher')

    Route.post('groups/:id', 'LessonsController.attachGroup')
    Route.delete('groups/:id', 'LessonsController.detachGroup')

    Route.post('classes/:id', 'LessonsController.attachClass')
    Route.delete('classes/:id', 'LessonsController.detachClass')

    Route.post('subjects/:id', 'LessonsController.attachSubject')
    Route.delete('subjects/:id', 'LessonsController.detachSubject')

    Route.post('classrooms/:id', 'LessonsController.attachClassroom')
    Route.delete('classrooms/:id', 'LessonsController.detachClassroom')
  })
    .prefix('lessons/:lesson_id')
    .where('lesson_id', /^[0-9]+$/)

  // Students
  Route.resource('students', 'StudentsController').apiOnly()
  Route.post('students/store_many', 'StudentsController.storeMany')

  // Subjects
  Route.resource('subjects', 'SubjectsController').apiOnly()

  // Teachers
  Route.resource('teachers', 'TeachersController').apiOnly()

  Route.group(() => {
    Route.post('subjects/:id', 'TeachersController.attachSubject')
    Route.delete('subjects/:id', 'TeachersController.detachSubject')
  })
    .prefix('teachers/:teacher_id')
    .where('teacher_id', /^[0-9]+$/)
}).middleware('auth')
