import Bouncer from '@ioc:Adonis/Addons/Bouncer'

export const { actions } = Bouncer

export const { policies } = Bouncer.registerPolicies({
  AdminPolicy: () => import('App/Policies/AdminPolicy'),
  StudentPolicy: () => import('App/Policies/StudentPolicy'),
  TeacherPolicy: () => import('App/Policies/TeacherPolicy'),
  ClassroomPolicy: () => import('App/Policies/ClassroomPolicy'),
  SubjectPolicy: () => import('App/Policies/SubjectPolicy'),
  ClassPolicy: () => import('App/Policies/ClassPolicy'),
  GroupPolicy: () => import('App/Policies/GroupPolicy'),
  LessonPolicy: () => import('App/Policies/LessonPolicy'),
})
