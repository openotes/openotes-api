import { DateTime } from 'luxon'
import { BaseModel, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Student from './Student'
import Teacher from './Teacher'

export default class Class extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public title: string

  @column()
  public description?: string

  @hasMany(() => Student)
  public students: HasMany<typeof Student>

  @hasMany(() => Teacher)
  public headTeachers: HasMany<typeof Teacher>
}
