import { DateTime } from 'luxon'
import { BaseModel, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Group from './Group'
import Teacher from './Teacher'
import Subject from './Subject'
import Classroom from './Classroom'
import Class from './Class'

export default class Lesson extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public title: string

  @column()
  public description?: string

  @column.dateTime()
  public startTimestamp: DateTime

  @column.dateTime()
  public endTimestamp: DateTime

  // Relations

  @manyToMany(() => Group)
  public groups: ManyToMany<typeof Group>

  @manyToMany(() => Teacher)
  public teachers: ManyToMany<typeof Teacher>

  @manyToMany(() => Subject)
  public subjects: ManyToMany<typeof Subject>

  @manyToMany(() => Classroom)
  public classrooms: ManyToMany<typeof Classroom>

  @manyToMany(() => Class)
  public classes: ManyToMany<typeof Class>
}
