import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Application from '@ioc:Adonis/Core/Application'

import { promises as fs } from 'fs'
import neatcsv from 'neat-csv'
import { DateTime } from 'luxon'

import User from 'App/Models/User'
import Student from 'App/Models/Student'
import StudentValidator from 'App/Validators/StudentValidator'
import Database from '@ioc:Adonis/Lucid/Database'

import { formatPaginatedData } from 'App/Helpers/pagination'
import StudentListValidator from 'App/Validators/StudentListValidator'

export default class StudentsController {
  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit, search, order_by, order_desc } = await request.validate(
      StudentListValidator
    )
    await bouncer.with('StudentPolicy').authorize('viewList')
    const paginator = await Student.query()
      .if(search, (query) => query.where('lastname', 'ilike', `%${search}%`))
      .if(!!order_by, (query) => query.orderBy(order_by!, order_desc ? 'desc' : 'asc')) // May change this to .match
      .paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('StudentPolicy').authorize('create')
    const { username, password, firstname, lastname, birthDate, gender } = await request.validate(
      StudentValidator
    )
    const student = await Database.transaction(async (trx) => {
      const user = await User.create(
        {
          username,
          password,
          role: 'student',
        },
        { client: trx }
      )
      const student = await Student.create(
        {
          firstname,
          lastname,
          birthDate,
          gender,
        },
        { client: trx }
      )
      student.useTransaction(trx)
      await student.related('user').associate(user)
      return student
    })
    return response.created(student)
  }

  public async storeMany({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('StudentPolicy').authorize('create')
    const { csv } = await request.validate({
      schema: schema.create({
        csv: schema.file({ extnames: ['csv'] }),
      }),
    })

    const fileName = `${Date.now()}.${csv.extname}`
    await csv.move(Application.tmpPath('csvs'), { name: fileName })
    const text = await fs.readFile(csv.filePath!, { encoding: 'utf-8' })
    const parsedCsv = await neatcsv(text)

    const users = await User.createMany(
      parsedCsv.map((record) => ({
        username: record.username,
        password: record.password,
      }))
    )

    const studentsCsv = parsedCsv.map((record) => {
      const relatedUser = users.find((user) => user.username === record.username)!
      return {
        firstname: record.firstname,
        lastname: record.lastname,
        birthDate: DateTime.fromSQL(record.birth_date),
        gender: record.gender,
        userId: relatedUser.id,
      }
    })

    const students = await Student.createMany(studentsCsv)

    fs.unlink(csv.filePath!)

    return response.created(students.length)
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const student = await Student.findOrFail(params.id)
    await bouncer.with('StudentPolicy').authorize('view', student)
    await student.load('user')
    return student
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const { username, password, firstname, lastname, birthDate, gender } = await request.validate(
      StudentValidator
    )
    const student = await Student.findOrFail(params.id)
    await bouncer.with('StudentPolicy').authorize('update', student)
    await student.load('user')

    await student.user.merge({ username, password }).save()
    return await student.merge({ firstname, lastname, birthDate, gender }).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const student = await Student.findOrFail(params.id)
    await bouncer.with('StudentPolicy').authorize('delete', student)
    await student.delete()
    return response.noContent()
  }
}
