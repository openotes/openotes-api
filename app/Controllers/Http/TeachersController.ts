import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import User from 'App/Models/User'
import Teacher from 'App/Models/Teacher'
import Subject from 'App/Models/Subject'
import TeacherValidator from 'App/Validators/TeacherValidator'
import PaginationValidator from 'App/Validators/PaginationValidator'
import Database from '@ioc:Adonis/Lucid/Database'

import { formatPaginatedData } from 'App/Helpers/pagination'

export default class TeachersController {
  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit } = await request.validate(PaginationValidator)
    await bouncer.with('TeacherPolicy').authorize('viewList')
    const paginator = await Teacher.query().paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('TeacherPolicy').authorize('create')
    const { username, password, firstname, lastname } = await request.validate(TeacherValidator)
    const teacher = await Database.transaction(async (trx) => {
      const user = await User.create(
        {
          username,
          password,
          role: 'teacher',
        },
        { client: trx }
      )
      const teacher = await Teacher.create(
        {
          firstname,
          lastname,
        },
        { client: trx }
      )
      teacher.useTransaction(trx)
      await teacher.related('user').associate(user)
      return teacher
    })
    return response.created(teacher)
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const teacher = await Teacher.findOrFail(params.id)
    await bouncer.with('TeacherPolicy').authorize('view', teacher)
    await teacher.load('user')
    await teacher.load('subjects')
    return teacher
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const { username, password, firstname, lastname, gender } = await request.validate(
      TeacherValidator
    )
    const teacher = await Teacher.findOrFail(params.id)
    await bouncer.with('TeacherPolicy').authorize('update', teacher)
    await teacher.load('user')

    await teacher.user.merge({ username, password }).save()
    return await teacher.merge({ firstname, lastname, gender }).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const teacher = await Teacher.findOrFail(params.id)
    await bouncer.with('TeacherPolicy').authorize('delete', teacher)
    await teacher.delete()
    return response.noContent()
  }

  public async attachSubject({ response, params, bouncer }: HttpContextContract) {
    const { id, teacher_id: teacherId } = params
    const subject = await Subject.findOrFail(id)
    const teacher = await Teacher.findOrFail(teacherId)
    await bouncer.with('TeacherPolicy').authorize('subjectOps', teacher)
    await teacher.related('subjects').save(subject)
    return response.noContent()
  }

  public async detachSubject({ response, params, bouncer }: HttpContextContract) {
    const { id, teacher_id: teacherId } = params
    const subject = await Subject.findOrFail(id)
    const teacher = await Teacher.findOrFail(teacherId)
    await bouncer.with('TeacherPolicy').authorize('subjectOps', teacher)
    await teacher.related('subjects').detach([subject.id])
    return response.noContent()
  }
}
