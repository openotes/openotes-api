import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Group from 'App/Models/Group'
import Student from 'App/Models/Student'
import TitleDescValidator from 'App/Validators/TitleDescValidator'
import PaginationValidator from 'App/Validators/PaginationValidator'

import { formatPaginatedData } from 'App/Helpers/pagination'

export default class GroupsController {
  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit } = await request.validate(PaginationValidator)
    await bouncer.with('GroupPolicy').authorize('viewList')
    const paginator = await Group.query().paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('GroupPolicy').authorize('create')
    const payload = await request.validate(TitleDescValidator)
    return response.created(await Group.create(payload))
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const group = await Group.findOrFail(params.id)
    await bouncer.with('GroupPolicy').authorize('view', group)
    await group.load('students')
    return group
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const data = await request.validate(TitleDescValidator)
    const group = await Group.findOrFail(params.id)
    await bouncer.with('GroupPolicy').authorize('update', group)
    return await group.merge(data).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const group = await Group.findOrFail(params.id)
    await bouncer.with('GroupPolicy').authorize('delete', group)
    await group.delete()
    return response.noContent()
  }

  public async attachStudent({ response, params, bouncer }: HttpContextContract) {
    const { id, group_id: groupId } = params
    const student = await Student.findOrFail(id)
    const group = await Group.findOrFail(groupId)
    await bouncer.with('GroupPolicy').authorize('studentOps', group)
    await group.related('students').save(student)
    return response.noContent()
  }

  public async detachStudent({ response, params, bouncer }: HttpContextContract) {
    const { id, group_id: groupId } = params
    const student = await Student.findOrFail(id)
    const group = await Group.findOrFail(groupId)
    await bouncer.with('GroupPolicy').authorize('studentOps', group)
    await group.related('students').detach([student.id])
    return response.noContent()
  }
}
