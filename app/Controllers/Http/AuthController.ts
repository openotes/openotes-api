import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import { DateTime } from 'luxon'

export default class AuthController {
  public async login({ request, auth }: HttpContextContract) {
    const { username, password } = await request.validate({
      schema: schema.create({
        username: schema.string(),
        password: schema.string(),
      }),
    })
    const token = await auth.attempt(username, password, { expiresIn: '10 days' })

    auth.user!.lastLogin = DateTime.local()
    await auth.user!.save()

    return token.toJSON()
  }
  public async logout({ response, auth }: HttpContextContract) {
    await auth.logout()
    return response.noContent()
  }
}
