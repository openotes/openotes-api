import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import { Exception } from '@adonisjs/core/build/standalone'

import Admin from 'App/Models/Admin'
import User from 'App/Models/User'
import AdminValidator from 'App/Validators/AdminValidator'
import PaginationValidator from 'App/Validators/PaginationValidator'

import { formatPaginatedData } from 'App/Helpers/pagination'

export default class AdminsController {
  private async createAdmin(payload: AdminValidator['schema']['props']): Promise<Admin> {
    const { username, password, name } = payload
    const admin = await Database.transaction(async (trx) => {
      const user = await User.create(
        {
          username,
          password,
          role: 'admin',
        },
        { client: trx }
      )
      const admin = await Admin.create(
        {
          name,
        },
        { client: trx }
      )
      admin.useTransaction(trx)
      await admin.related('user').associate(user)
      return admin
    })
    return admin
  }

  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit } = await request.validate(PaginationValidator)
    await bouncer.with('AdminPolicy').authorize('viewList')
    const paginator = await Admin.query().paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('AdminPolicy').authorize('create')
    const payload = await request.validate(AdminValidator)
    return response.created(await this.createAdmin(payload))
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const admin = await Admin.findOrFail(params.id)
    await bouncer.with('AdminPolicy').authorize('view', admin)
    await admin.load('user')
    return admin
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const { username, password, name } = await request.validate(AdminValidator)
    const admin = await Admin.findOrFail(params.id)

    await bouncer.with('AdminPolicy').authorize('update', admin)
    await admin.load('user')

    await admin.user.merge({ username, password }).save()
    return await admin.merge({ name }).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const admin = await Admin.findOrFail(params.id)
    await bouncer.with('AdminPolicy').authorize('delete', admin)
    await admin.delete()
    return response.noContent()
  }

  private async canInit(): Promise<boolean> {
    const count = await Admin.query().pojo<{ total: string }>().count('id as total')
    return parseInt(count[0].total) === 0
  }

  public async getInit() {
    return { can_init: await this.canInit() }
  }

  public async postInit({ request }: HttpContextContract) {
    if (await this.canInit()) {
      const payload = await request.validate(AdminValidator)
      return await this.createAdmin(payload)
    } else {
      throw new Exception('App is already initialized', 401, 'E_ALREADY_INITIALIZED')
    }
  }
}
