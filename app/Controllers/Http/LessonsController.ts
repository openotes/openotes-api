import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Lesson from 'App/Models/Lesson'
import Teacher from 'App/Models/Teacher'
import Group from 'App/Models/Group'
import Class from 'App/Models/Class'
import Classroom from 'App/Models/Classroom'
import Subject from 'App/Models/Subject'
import LessonValidator from 'App/Validators/LessonValidator'
import PaginationValidator from 'App/Validators/PaginationValidator'

import { formatPaginatedData } from 'App/Helpers/pagination'

export default class LessonsController {
  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit } = await request.validate(PaginationValidator)
    await bouncer.with('LessonPolicy').authorize('viewList')
    const paginator = await Lesson.query().paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('LessonPolicy').authorize('create')
    const payload = await request.validate(LessonValidator)
    return response.created(await Lesson.create(payload))
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const lesson = await Lesson.findOrFail(params.id)
    await bouncer.with('LessonPolicy').authorize('view', lesson)
    await lesson.load('groups')
    await lesson.load('teachers')
    await lesson.load('subjects')
    await lesson.load('classrooms')
    await lesson.load('classes')
    return lesson
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const payload = await request.validate(LessonValidator)
    const lesson = await Lesson.findOrFail(params.id)
    await bouncer.with('LessonPolicy').authorize('update', lesson)
    return await lesson.merge(payload).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const lesson = await Lesson.findOrFail(params.id)
    await bouncer.with('LessonPolicy').authorize('delete', lesson)
    await lesson.delete()
    return response.noContent()
  }

  public async attachTeacher({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const teacher = await Teacher.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('teacherOps', lesson)
    await lesson.related('teachers').attach([teacher.id])
    return response.noContent()
  }

  public async detachTeacher({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const teacher = await Teacher.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('teacherOps', lesson)
    await lesson.related('teachers').detach([teacher.id])
    return response.noContent()
  }

  public async attachGroup({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const group = await Group.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('groupOps', lesson)
    await lesson.related('groups').attach([group.id])
    return response.noContent()
  }

  public async detachGroup({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const group = await Group.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('groupOps', lesson)
    await lesson.related('groups').detach([group.id])
    return response.noContent()
  }

  public async attachClass({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const theClass = await Class.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('classOps', lesson)
    await lesson.related('classes').attach([theClass.id])
    return response.noContent()
  }

  public async detachClass({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const theClass = await Class.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('classOps', lesson)
    await lesson.related('classes').detach([theClass.id])
    return response.noContent()
  }

  public async attachSubject({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const subject = await Subject.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('subjectOps', lesson)
    await lesson.related('subjects').attach([subject.id])
    return response.noContent()
  }

  public async detachSubject({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const subject = await Subject.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('subjectOps', lesson)
    await lesson.related('subjects').detach([subject.id])
    return response.noContent()
  }

  public async attachClassroom({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const classroom = await Classroom.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('classroomOps', lesson)
    await lesson.related('classrooms').attach([classroom.id])
    return response.noContent()
  }

  public async detachClassroom({ response, params, bouncer }: HttpContextContract) {
    const { id, lesson_id: lessonId } = params
    const classroom = await Classroom.findOrFail(id)
    const lesson = await Lesson.findOrFail(lessonId)
    await bouncer.with('LessonPolicy').authorize('classroomOps', lesson)
    await lesson.related('classrooms').detach([classroom.id])
    return response.noContent()
  }
}
