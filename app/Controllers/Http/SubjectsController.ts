import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Subject from 'App/Models/Subject'
import TitleDescValidator from 'App/Validators/TitleDescValidator'
import PaginationValidator from 'App/Validators/PaginationValidator'

import { formatPaginatedData } from 'App/Helpers/pagination'

export default class SubjectsController {
  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit } = await request.validate(PaginationValidator)
    await bouncer.with('SubjectPolicy').authorize('viewList')
    const paginator = await Subject.query().paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('SubjectPolicy').authorize('create')
    const payload = await request.validate(TitleDescValidator)
    return response.created(await Subject.create(payload))
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const subject = await Subject.findOrFail(params.id)
    await bouncer.with('SubjectPolicy').authorize('view', subject)
    return subject
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const payload = await request.validate(TitleDescValidator)
    const subject = await Subject.findOrFail(params.id)
    await bouncer.with('SubjectPolicy').authorize('update', subject)
    return await subject.merge(payload).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const subject = await Subject.findOrFail(params.id)
    await bouncer.with('SubjectPolicy').authorize('delete', subject)
    await subject.delete()
    return response.noContent()
  }
}
