import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Class from 'App/Models/Class'
import Student from 'App/Models/Student'
import Teacher from 'App/Models/Teacher'
import TitleDescValidator from 'App/Validators/TitleDescValidator'
import PaginationValidator from 'App/Validators/PaginationValidator'

import { formatPaginatedData } from 'App/Helpers/pagination'

export default class ClassesController {
  public async index({ request, bouncer }: HttpContextContract) {
    const { page, limit } = await request.validate(PaginationValidator)
    await bouncer.with('ClassPolicy').authorize('viewList')
    const paginator = await Class.query().paginate(page ?? 1, limit ?? 15)
    return formatPaginatedData(paginator)
  }

  public async store({ response, request, bouncer }: HttpContextContract) {
    await bouncer.with('ClassPolicy').authorize('create')
    const { title, description } = await request.validate(TitleDescValidator)
    return response.created(
      await Class.create({
        title,
        description,
      })
    )
  }

  public async show({ params, bouncer }: HttpContextContract) {
    const theClass = await Class.findOrFail(params.id)
    await bouncer.with('ClassPolicy').authorize('view', theClass)
    await theClass.load('students')
    await theClass.load('headTeachers')
    return theClass
  }

  public async update({ params, request, bouncer }: HttpContextContract) {
    const data = await request.validate(TitleDescValidator)
    const theClass = await Class.findOrFail(params.id)
    await bouncer.with('ClassPolicy').authorize('update', theClass)
    return await theClass.merge(data).save()
  }

  public async destroy({ response, params, bouncer }: HttpContextContract) {
    const theClass = await Class.findOrFail(params.id)
    await bouncer.with('ClassPolicy').authorize('delete', theClass)
    await theClass.delete()
    return response.noContent()
  }

  public async attachStudent({ response, params, bouncer }: HttpContextContract) {
    const { id, class_id: classId } = params
    const theClass = await Class.findOrFail(classId)
    const student = await Student.findOrFail(id)
    await bouncer.with('ClassPolicy').authorize('studentOps', theClass)
    await theClass.related('students').save(student)
    return response.noContent()
  }

  public async detachStudent({ response, params, bouncer }: HttpContextContract) {
    const { id, class_id: classId } = params
    const theClass = await Class.findOrFail(classId)
    const student = await Student.findOrFail(id)
    await bouncer.with('ClassPolicy').authorize('studentOps', theClass)
    await theClass.related('students').query().where('id', student.id).update({ class_id: null })
    return response.noContent()
  }

  public async attachTeacher({ response, params, bouncer }: HttpContextContract) {
    const { id, class_id: classId } = params
    const theClass = await Class.findOrFail(classId)
    const teacher = await Teacher.findOrFail(id)
    await bouncer.with('ClassPolicy').authorize('teacherOps', theClass)
    await theClass.related('headTeachers').save(teacher)
    return response.noContent()
  }

  public async detachTeacher({ response, params, bouncer }: HttpContextContract) {
    const { id, class_id: classId } = params
    const theClass = await Class.findOrFail(classId)
    const teacher = await Teacher.findOrFail(id)
    await bouncer.with('ClassPolicy').authorize('teacherOps', theClass)
    await theClass
      .related('headTeachers')
      .query()
      .where('id', teacher.id)
      .update({ class_id: null })
    return response.noContent()
  }
}
