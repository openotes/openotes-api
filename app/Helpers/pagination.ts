import { LucidRow, ModelPaginatorContract } from '@ioc:Adonis/Lucid/Model'

export interface PaginatedData<T> {
  data: T[]
  total: number
  limit: number
  page: number
  last_page: number
}

export function formatPaginatedData<T extends LucidRow>(
  paginator: ModelPaginatorContract<T>
): PaginatedData<T> {
  return {
    data: paginator.all(),
    total: paginator.total,
    limit: paginator.perPage,
    page: paginator.currentPage,
    last_page: paginator.lastPage,
  }
}
