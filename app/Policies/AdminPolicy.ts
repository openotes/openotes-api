import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Admin from 'App/Models/Admin'

export default class AdminPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, _admin: Admin) {
    return user.role === 'admin'
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _admin: Admin) {
    return user.role === 'admin'
  }
  public async delete(user: User, _admin: Admin) {
    return user.role === 'admin'
  }
}
