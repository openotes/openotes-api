import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Class from 'App/Models/Class'

export default class ClassPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, theClass: Class) {
    switch (user.role) {
      case 'admin':
        return true
      case 'teacher': {
        // Authorize the teacher if he/she is a head teacher of the class
        await theClass.load('headTeachers')
        const teachersUserId = theClass.headTeachers.map((teacher) => teacher.userId)
        return teachersUserId.includes(user.id)
      }
      case 'student':
        return false
    }
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _theClass: Class) {
    return user.role === 'admin'
  }
  public async delete(user: User, _theClass: Class) {
    return user.role === 'admin'
  }
  public async studentOps(user: User, _theClass: Class) {
    return user.role === 'admin'
  }
  public async teacherOps(user: User, _theClass: Class) {
    return user.role === 'admin'
  }
}
