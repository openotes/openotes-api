import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'

import User from 'App/Models/User'
import Student from 'App/Models/Student'

export default class StudentPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, student: Student) {
    switch (user.role) {
      case 'admin':
      case 'teacher':
        return true
      case 'student':
        await student.load('user')
        return student.user.id === user.id
    }
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _student: Student) {
    return user.role === 'admin'
  }
  public async delete(user: User, _student: Student) {
    return user.role === 'admin'
  }
}
