import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Teacher from 'App/Models/Teacher'

export default class TeacherPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, teacher: Teacher) {
    switch (user.role) {
      case 'admin':
        return true
      case 'teacher':
        await teacher.load('user')
        return teacher.user.id === user.id
      case 'student':
        return false
    }
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _teacher: Teacher) {
    return user.role === 'admin'
  }
  public async delete(user: User, _teacher: Teacher) {
    return user.role === 'admin'
  }
  public async subjectOps(user: User, teacher: Teacher) {
    switch (user.role) {
      case 'admin':
        return true
      case 'teacher':
        await teacher.load('user')
        return teacher.user.id === user.id
      case 'student':
        return false
    }
  }
}
