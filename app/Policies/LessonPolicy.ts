import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Lesson from 'App/Models/Lesson'

export default class LessonPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, lesson: Lesson) {
    switch (user.role) {
      case 'admin':
        return true
      case 'teacher': {
        await lesson.load('teachers')
        const teachersUserId = lesson.teachers.map((teacher) => teacher.userId)
        return teachersUserId.includes(user.id)
      }
      case 'student': {
        // Authorize the student if he/she is in lesson trough a group or a class
        // TODO: tests
        await lesson.load('groups', (query) => query.preload('students'))
        await lesson.load('classes', (query) => query.preload('students'))
        // Retrieve userId from all students in all groups
        const studentsUserIdFromGroups = lesson.groups.reduce<number[]>((ids, group) => {
          const studentsUserId = group.students.map((student) => student.userId)
          return ids.concat(studentsUserId)
        }, [])
        // Retrieve userId from all students in all classes
        const studentsUserIdFromClasses = lesson.classes.reduce<number[]>((ids, theClass) => {
          const studentsUserId = theClass.students.map((student) => student.userId)
          return ids.concat(studentsUserId)
        }, [])
        // Concat and test
        const studentsUserId = studentsUserIdFromGroups.concat(studentsUserIdFromClasses)
        return studentsUserId.includes(user.id)
      }
    }
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
  public async delete(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
  public async teacherOps(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
  public async groupOps(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
  public async classOps(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
  public async subjectOps(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
  public async classroomOps(user: User, _lesson: Lesson) {
    return user.role === 'admin'
  }
}
