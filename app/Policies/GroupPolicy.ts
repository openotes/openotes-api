import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Group from 'App/Models/Group'

export default class GroupPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, _group: Group) {
    return user.role === 'admin'
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _group: Group) {
    return user.role === 'admin'
  }
  public async delete(user: User, _group: Group) {
    return user.role === 'admin'
  }
  public async studentOps(user: User, _group: Group) {
    return user.role === 'admin'
  }
}
