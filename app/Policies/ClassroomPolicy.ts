import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Classroom from 'App/Models/Classroom'

export default class ClassroomPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, _classroom: Classroom) {
    return user.role === 'admin'
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _classroom: Classroom) {
    return user.role === 'admin'
  }
  public async delete(user: User, _classroom: Classroom) {
    return user.role === 'admin'
  }
}
