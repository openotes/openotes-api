import { BasePolicy } from '@ioc:Adonis/Addons/Bouncer'
import User from 'App/Models/User'
import Subject from 'App/Models/Subject'

export default class SubjectPolicy extends BasePolicy {
  public async viewList(user: User) {
    return user.role === 'admin'
  }
  public async view(user: User, _subject: Subject) {
    return user.role === 'admin'
  }
  public async create(user: User) {
    return user.role === 'admin'
  }
  public async update(user: User, _subject: Subject) {
    return user.role === 'admin'
  }
  public async delete(user: User, _subject: Subject) {
    return user.role === 'admin'
  }
}
