import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AdminValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    username: schema.string({ trim: true }),
    password: schema.string.optional(),
    name: schema.string({ trim: true }),
  })

  public messages = {}

  public cacheKey = this.ctx.routeKey
}
