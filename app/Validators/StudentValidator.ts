import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class StudentValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    username: schema.string({ trim: true }),
    password: schema.string.optional(),
    firstname: schema.string({ trim: true }),
    lastname: schema.string({ trim: true }),
    birthDate: schema.date(),
    gender: schema.string.optional(),
  })

  public messages = {}

  public cacheKey = this.ctx.routeKey
}
