import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class TeacherValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    username: schema.string({ trim: true }),
    password: schema.string.optional(),
    firstname: schema.string({ trim: true }),
    lastname: schema.string({ trim: true }),
    gender: schema.string.optional(),
  })

  public messages = {}

  public cacheKey = this.ctx.routeKey
}
