import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class LessonValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    title: schema.string({ trim: true }),
    description: schema.string.optional(),
    startTimestamp: schema.date({}, [rules.beforeField('endTimestamp')]),
    endTimestamp: schema.date(),
  })

  public messages = {}

  public cacheKey = this.ctx.routeKey
}
