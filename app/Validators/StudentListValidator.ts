import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class StudentListValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    page: schema.number.optional([rules.unsigned()]),
    limit: schema.number.optional([rules.unsigned()]),
    search: schema.string.optional({ trim: true }),
    order_by: schema.enum.optional(['firstname', 'lastname', 'birth_date'] as const),
    order_desc: schema.boolean.optional(),
  })

  public messages = {}
}
