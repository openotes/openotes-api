import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import { AdminFactory, LessonFactory } from 'Database/factories'

export default class SandboxSeeder extends BaseSeeder {
  public async run() {
    await LessonFactory.with('classes', 1)
      .with('groups', 2, (group) =>
        group.with('students', 5, (student) =>
          student.with('user', 1, (user) => user.apply('student'))
        )
      )
      .with('teachers', 2, (teachers) =>
        teachers.with('user', 1, (user) => user.apply('teacher')).with('subjects', 2)
      )
      .with('subjects', 2)
      .with('classrooms', 3)
      .createMany(3)
    await AdminFactory.with('user', 1, (user) => user.apply('admin')).createMany(2)
  }
}
