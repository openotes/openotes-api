import Factory from '@ioc:Adonis/Lucid/Factory'
import { DateTime } from 'luxon'

import Admin from 'App/Models/Admin'
import Student from 'App/Models/Student'
import Teacher from 'App/Models/Teacher'
import User from 'App/Models/User'
import Class from 'App/Models/Class'
import Classroom from 'App/Models/Classroom'
import Group from 'App/Models/Group'
import Subject from 'App/Models/Subject'
import Lesson from 'App/Models/Lesson'

export const UserFactory = Factory.define(User, ({ faker }) => ({
  username: faker.internet.userName(),
  password: 'test',
}))
  .state('admin', (user) => (user.role = 'admin'))
  .state('student', (user) => (user.role = 'student'))
  .state('teacher', (user) => (user.role = 'teacher'))
  .build()

export const StudentFactory = Factory.define(Student, ({ faker }) => ({
  firstname: faker.name.firstName(),
  lastname: faker.name.lastName(),
  birthDate: DateTime.local(),
}))
  .relation('user', () => UserFactory)
  .build()

export const AdminFactory = Factory.define(Admin, ({ faker }) => ({
  name: faker.fake('{{name.firstName}} {{name.lastName}}'),
}))
  .relation('user', () => UserFactory)
  .build()

export const TeacherFactory = Factory.define(Teacher, ({ faker }) => ({
  firstname: faker.name.firstName(),
  lastname: faker.name.lastName(),
}))
  .relation('user', () => UserFactory)
  .relation('subjects', () => SubjectFactory)
  .build()

export const ClassFactory = Factory.define(Class, ({ faker }) => ({
  title: faker.lorem.word(),
  description: faker.lorem.sentences(),
}))
  .relation('headTeachers', () => TeacherFactory)
  .relation('students', () => StudentFactory)
  .build()

export const ClassroomFactory = Factory.define(Classroom, ({ faker }) => ({
  title: faker.lorem.word(),
  description: faker.lorem.sentences(),
})).build()

export const GroupFactory = Factory.define(Group, ({ faker }) => ({
  title: faker.lorem.word(),
  description: faker.lorem.sentences(),
}))
  .relation('students', () => StudentFactory)
  .build()

export const SubjectFactory = Factory.define(Subject, ({ faker }) => ({
  title: faker.lorem.word(),
  description: faker.lorem.sentences(),
})).build()

export const LessonFactory = Factory.define(Lesson, ({ faker }) => ({
  title: faker.lorem.word(),
  description: faker.lorem.sentences(),
  startTimestamp: DateTime.fromJSDate(faker.date.past()),
  endTimestamp: DateTime.fromJSDate(faker.date.future()),
}))
  .relation('groups', () => GroupFactory)
  .relation('classes', () => ClassFactory)
  .relation('classrooms', () => ClassroomFactory)
  .relation('teachers', () => TeacherFactory)
  .relation('subjects', () => SubjectFactory)
  .build()
