import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class GroupLesson extends BaseSchema {
  protected tableName = 'group_lesson'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('lesson_id').unsigned().references('id').inTable('lessons').onDelete('CASCADE')
      table.integer('group_id').unsigned().references('id').inTable('groups').onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
