import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TeacherLesson extends BaseSchema {
  protected tableName = 'lesson_teacher'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('lesson_id').unsigned().references('id').inTable('lessons').onDelete('CASCADE')
      table
        .integer('teacher_id')
        .unsigned()
        .references('id')
        .inTable('teachers')
        .onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
