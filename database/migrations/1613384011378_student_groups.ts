import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class StudentGroup extends BaseSchema {
  protected tableName = 'group_student'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table
        .integer('student_id')
        .unsigned()
        .references('id')
        .inTable('students')
        .onDelete('CASCADE')
      table.integer('group_id').unsigned().references('id').inTable('groups').onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
