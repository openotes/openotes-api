import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Teachers extends BaseSchema {
  protected tableName = 'teachers'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
      table.string('firstname').notNullable()
      table.string('lastname').notNullable()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('gender')
      table.index(['lastname'])
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
