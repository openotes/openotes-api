import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ClassLessons extends BaseSchema {
  protected tableName = 'class_lesson'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('lesson_id').unsigned().references('id').inTable('lessons').onDelete('CASCADE')
      table.integer('class_id').unsigned().references('id').inTable('classes').onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
