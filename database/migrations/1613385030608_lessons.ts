import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Lessons extends BaseSchema {
  protected tableName = 'lessons'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
      table.string('title').notNullable()
      table.text('description')
      table.timestamp('start_timestamp', { useTz: true }).notNullable()
      table.timestamp('end_timestamp', { useTz: true }).notNullable()
      table.index(['title'])
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
