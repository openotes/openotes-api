import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Admins extends BaseSchema {
  protected tableName = 'admins'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
      table.string('name').notNullable()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.index(['name'])
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
