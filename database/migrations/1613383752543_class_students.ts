import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Students extends BaseSchema {
  protected tableName = 'students'

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.integer('class_id').unsigned().references('id').inTable('classes')
    })
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('class_id')
    })
  }
}
