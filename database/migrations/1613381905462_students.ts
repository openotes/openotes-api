import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Students extends BaseSchema {
  protected tableName = 'students'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
      table.string('firstname').notNullable()
      table.string('lastname').notNullable()
      table.date('birth_date').notNullable()
      table.string('gender')
      table.boolean('present').notNullable().defaultTo(true)
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.index(['lastname'])
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
