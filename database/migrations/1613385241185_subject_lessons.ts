import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class SubjectLesson extends BaseSchema {
  protected tableName = 'lesson_subject'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('lesson_id').unsigned().references('id').inTable('lessons').onDelete('CASCADE')
      table
        .integer('subject_id')
        .unsigned()
        .references('id')
        .inTable('subjects')
        .onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
