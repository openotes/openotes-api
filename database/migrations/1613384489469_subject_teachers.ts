import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class SubjectTeacher extends BaseSchema {
  protected tableName = 'subject_teacher'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table
        .integer('teacher_id')
        .unsigned()
        .references('id')
        .inTable('teachers')
        .onDelete('CASCADE')
      table
        .integer('subject_id')
        .unsigned()
        .references('id')
        .inTable('subjects')
        .onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
