import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ClassroomLesson extends BaseSchema {
  protected tableName = 'classroom_lesson'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('lesson_id').unsigned().references('id').inTable('lessons').onDelete('CASCADE')
      table
        .integer('classroom_id')
        .unsigned()
        .references('id')
        .inTable('classrooms')
        .onDelete('CASCADE')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
