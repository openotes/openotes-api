import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Teachers extends BaseSchema {
  protected tableName = 'teachers'

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.integer('class_id').unsigned().references('id').inTable('classes')
    })
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn('class_id')
    })
  }
}
