import test from 'japa'
import supertest from 'supertest'
import { baseUrl, createFakeAdmin, loginAndGetToken, logout, paginationKeys } from '../test-helpers'

import Admin from 'App/Models/Admin'

let token

test.group('Admin', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all admins', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/admins')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get an admin', async (assert) => {
    const { id } = await createFakeAdmin()
    const { body } = await supertest(baseUrl)
      .get(`/admins/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the admin does not exists', async () => {
    const admin = await Admin.find(1)
    if (admin) await admin.delete()
    await supertest(baseUrl)
      .get('/admins/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create an admin', async (assert) => {
    const data = { username: 'example', password: 'esfezf', name: 'Test' }
    const { body } = await supertest(baseUrl)
      .post('/admins')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdAdmin = await Admin.find(body.id)
    assert.isNotNull(createdAdmin)
    await createdAdmin?.load('user')
    assert.equal(createdAdmin?.user.username, data.username)
  })

  test('should update an admin', async (assert) => {
    const admin = await createFakeAdmin()

    const data = { username: 'example', name: 'Test' }
    await supertest(baseUrl)
      .patch(`/admins/${admin.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await admin.refresh()
    await admin.load('user')

    assert.equal(admin.name, data.name)
    assert.equal(admin.user.username, data.username)
  })

  test('should delete an admin', async (assert) => {
    const admin = await createFakeAdmin()

    await supertest(baseUrl)
      .delete(`/admins/${admin.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedAdmin = await Admin.find(admin.id)
    assert.isNull(deletedAdmin)
  })
})
