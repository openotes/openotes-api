import test from 'japa'
import supertest from 'supertest'
import {
  baseUrl,
  loginAsStudent,
  loginAsTeacher,
  logout,
  createFakeTeacher,
} from '../../test-helpers'

import { SubjectFactory } from 'Database/factories'

let teacherToken
let teacherId
let studentToken

test.group('Class policies', (group) => {
  group.before(async () => {
    const teacherResult = await loginAsTeacher()
    teacherToken = teacherResult.token
    teacherId = teacherResult.id
    const studentResult = await loginAsStudent()
    studentToken = studentResult.token
  })

  group.after(async () => {
    await logout(teacherToken)
    await logout(studentToken)
  })

  test('should authorize view for teacher if he has the same id', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get(`/teachers/${teacherId}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, teacherId)
  })

  test('should unauthorize view for student', async () => {
    const teacher = await createFakeTeacher()

    await supertest(baseUrl)
      .get(`/teachers/${teacher.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })

  test('should authorize subject operations for teacher if he has the same id', async () => {
    const subject = await SubjectFactory.create()

    await supertest(baseUrl)
      .post(`/teachers/${teacherId}/subjects/${subject.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect(204)

    await supertest(baseUrl)
      .delete(`/teachers/${teacherId}/subjects/${subject.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect(204)
  })

  test('should unauthorize subject operations for student', async () => {
    const subject = await SubjectFactory.create()
    const teacher = await createFakeTeacher()

    await supertest(baseUrl)
      .post(`/teachers/${teacher.id}/subjects/${subject.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)

    await supertest(baseUrl)
      .delete(`/teachers/${teacher.id}/subjects/${subject.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })
})
