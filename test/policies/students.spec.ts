import test from 'japa'
import supertest from 'supertest'
import {
  baseUrl,
  loginAsStudent,
  loginAsTeacher,
  logout,
  createFakeStudent,
} from '../../test-helpers'

let teacherToken
let studentToken
let studentId

test.group('Class policies', (group) => {
  group.before(async () => {
    const teacherResult = await loginAsTeacher()
    teacherToken = teacherResult.token
    const studentResult = await loginAsStudent()
    studentToken = studentResult.token
    studentId = studentResult.id
  })

  group.after(async () => {
    await logout(teacherToken)
    await logout(studentToken)
  })

  test('should authorize view for teacher', async (assert) => {
    const student = await createFakeStudent()

    const { body } = await supertest(baseUrl)
      .get(`/students/${student.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, student.id)
  })

  test('should authorize view for student if he has the same id', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get(`/students/${studentId}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, studentId)
  })

  test('should unauthorize view for student', async () => {
    const student = await createFakeStudent()

    await supertest(baseUrl)
      .get(`/students/${student.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })
})
