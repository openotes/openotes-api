import test from 'japa'
import supertest from 'supertest'
import { baseUrl, loginAsStudent, loginAsTeacher, logout } from '../../test-helpers'

import Teacher from 'App/Models/Teacher'

import { ClassFactory } from 'Database/factories'

let teacherToken
let teacherId
let studentToken

test.group('Class policies', (group) => {
  group.before(async () => {
    const teacherResult = await loginAsTeacher()
    teacherId = teacherResult.id
    teacherToken = teacherResult.token
    const studentResult = await loginAsStudent()
    studentToken = studentResult.token
  })

  group.after(async () => {
    await logout(teacherToken)
    await logout(studentToken)
  })

  test('should authorize view if teacher is head teacher', async (assert) => {
    const theClass = await ClassFactory.create()
    const teacher = await Teacher.findOrFail(teacherId)

    await theClass.related('headTeachers').save(teacher)

    const { body } = await supertest(baseUrl)
      .get(`/classes/${theClass.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, theClass.id)
  })

  test('should unauthorize view if teacher is not head teacher', async () => {
    const theClass = await ClassFactory.create()

    await supertest(baseUrl)
      .get(`/classes/${theClass.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })

  test('should unauthorize view for student', async () => {
    const theClass = await ClassFactory.create()

    await supertest(baseUrl)
      .get(`/classes/${theClass.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })
})
