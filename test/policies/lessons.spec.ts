import test from 'japa'
import supertest from 'supertest'
import { baseUrl, loginAsStudent, loginAsTeacher, logout } from '../../test-helpers'

import Student from 'App/Models/Student'
import Teacher from 'App/Models/Teacher'
import { ClassFactory, GroupFactory, LessonFactory } from 'Database/factories'

let teacherToken
let teacherId
let studentToken
let studentId

test.group('Lesson policies', (group) => {
  group.before(async () => {
    const teacherResult = await loginAsTeacher()
    teacherId = teacherResult.id
    teacherToken = teacherResult.token
    const studentResult = await loginAsStudent()
    studentId = studentResult.id
    studentToken = studentResult.token
  })

  group.after(async () => {
    await logout(teacherToken)
    await logout(studentToken)
  })

  test('should authorize view if teacher is in lesson', async (assert) => {
    const lesson = await LessonFactory.create()
    const teacher = await Teacher.findOrFail(teacherId)

    await lesson.related('teachers').save(teacher)

    const { body } = await supertest(baseUrl)
      .get(`/lessons/${lesson.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, lesson.id)
  })

  test('should unauthorize view if teacher is not in lesson', async () => {
    const lesson = await LessonFactory.create()

    await supertest(baseUrl)
      .get(`/lessons/${lesson.id}`)
      .auth(teacherToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })

  test('should authorize view if student is in group in lesson', async (assert) => {
    const lesson = await LessonFactory.create()
    const group = await GroupFactory.create()
    const student = await Student.findOrFail(studentId)

    await lesson.related('groups').save(group)
    await group.related('students').save(student)

    const { body } = await supertest(baseUrl)
      .get(`/lessons/${lesson.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, lesson.id)
  })

  test('should authorize view if student is in class in lesson', async (assert) => {
    const lesson = await LessonFactory.create()
    const theClass = await ClassFactory.create()
    const student = await Student.findOrFail(studentId)

    await lesson.related('classes').save(theClass)
    await theClass.related('students').save(student)

    const { body } = await supertest(baseUrl)
      .get(`/lessons/${lesson.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, lesson.id)
  })

  test('should unauthorize view if student is not in group nor in class in lesson', async () => {
    const lesson = await LessonFactory.create()
    await supertest(baseUrl)
      .get(`/lessons/${lesson.id}`)
      .auth(studentToken, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(403)
  })
})
