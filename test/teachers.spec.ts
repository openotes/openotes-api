import test from 'japa'
import supertest from 'supertest'
import {
  baseUrl,
  createFakeTeacher,
  loginAndGetToken,
  logout,
  paginationKeys,
} from '../test-helpers'

import Teacher from 'App/Models/Teacher'
import { SubjectFactory } from 'Database/factories'

let token

test.group('Teacher', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all teachers', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/teachers')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)
    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a teacher', async (assert) => {
    const { id } = await createFakeTeacher()
    const { body } = await supertest(baseUrl)
      .get(`/teachers/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the teacher does not exists', async () => {
    const teacher = await Teacher.find(1)
    if (teacher) await teacher.delete()
    await supertest(baseUrl)
      .get('/teachers/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a teacher', async (assert) => {
    const data = { username: 'example', password: 'esfezf', firstname: 'Fez', lastname: 'Foo' }
    const { body } = await supertest(baseUrl)
      .post('/teachers')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdTeacher = await Teacher.find(body.id)
    assert.isNotNull(createdTeacher)
    await createdTeacher?.load('user')
    assert.equal(createdTeacher?.user.username, data.username)
  })

  test('should update a teacher', async (assert) => {
    const teacher = await createFakeTeacher()

    const data = { username: 'example', firstname: 'New', lastname: 'AlsoNew' }
    await supertest(baseUrl)
      .patch(`/teachers/${teacher.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await teacher.refresh()
    await teacher.load('user')

    assert.equal(teacher.firstname, data.firstname)
    assert.equal(teacher.lastname, data.lastname)
    assert.equal(teacher.user.username, data.username)
  })

  test('should delete a teacher', async (assert) => {
    const teacher = await createFakeTeacher()

    await supertest(baseUrl)
      .delete(`/teachers/${teacher.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedTeacher = await Teacher.find(teacher.id)
    assert.isNull(deletedTeacher)
  })

  test('should attach subject', async (assert) => {
    const teacher = await createFakeTeacher()
    const subject = await SubjectFactory.create()

    await supertest(baseUrl)
      .post(`/teachers/${teacher.id}/subjects/${subject.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await teacher.refresh()
    await teacher.load('subjects')
    const subjectIds = teacher.subjects.map((subject) => subject.id)
    assert.include(subjectIds, subject.id)
  })

  test('should detach subject', async (assert) => {
    const teacher = await createFakeTeacher()
    const subject = await SubjectFactory.create()
    await teacher.related('subjects').save(subject)

    await supertest(baseUrl)
      .delete(`/teachers/${teacher.id}/subjects/${subject.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await teacher.refresh()
    await teacher.load('subjects')
    const subjectIds = teacher.subjects.map((subject) => subject.id)
    assert.notInclude(subjectIds, subject.id)
  })
})
