import test from 'japa'
import supertest from 'supertest'
import { baseUrl, loginAndGetToken, logout, paginationKeys } from '../test-helpers'

import Lesson from 'App/Models/Lesson'
import { DateTime } from 'luxon'
import {
  ClassFactory,
  ClassroomFactory,
  GroupFactory,
  LessonFactory,
  SubjectFactory,
  TeacherFactory,
} from 'Database/factories'

let token

test.group('Lesson', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all lessons', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/lessons')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a lesson', async (assert) => {
    const { id } = await LessonFactory.create()
    const { body } = await supertest(baseUrl)
      .get(`/lessons/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the lesson does not exists', async () => {
    const lesson = await Lesson.find(1)
    if (lesson) await lesson.delete()

    await supertest(baseUrl)
      .get('/lessons/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a lesson', async (assert) => {
    const data = {
      title: 'efzef',
      description: 'mmmm',
      startTimestamp: DateTime.local(),
      endTimestamp: DateTime.local().plus({ hour: 1 }),
    }
    const { body } = await supertest(baseUrl)
      .post('/lessons')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdLesson = await Lesson.find(body.id)
    assert.isNotNull(createdLesson)
    assert.equal(createdLesson?.title, data.title)
    assert.equal(createdLesson?.description, data.description)
  })

  test('should update a lesson', async (assert) => {
    const lesson = await LessonFactory.create()

    const data = {
      title: 'example',
      description: 'Test',
      startTimestamp: DateTime.local(),
      endTimestamp: DateTime.local().plus({ hour: 1 }),
    }
    await supertest(baseUrl)
      .patch(`/lessons/${lesson.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await lesson.refresh()

    assert.equal(lesson.title, data.title)
    assert.equal(lesson.description, data.description)
  })

  test('should delete a lesson', async (assert) => {
    const lesson = await LessonFactory.create()

    await supertest(baseUrl)
      .delete(`/lessons/${lesson.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedLesson = await Lesson.find(lesson.id)
    assert.isNull(deletedLesson)
  })

  test('should attach a group', async (assert) => {
    const lesson = await LessonFactory.create()
    const group = await GroupFactory.create()

    await supertest(baseUrl)
      .post(`/lessons/${lesson.id}/groups/${group.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('groups')
    const groupIds = lesson.groups.map((group) => group.id)
    assert.include(groupIds, group.id)
  })

  test('should detach a group', async (assert) => {
    const lesson = await LessonFactory.create()
    const group = await GroupFactory.create()
    await lesson.related('groups').save(group)

    await supertest(baseUrl)
      .delete(`/lessons/${lesson.id}/groups/${group.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('groups')
    const groupIds = lesson.groups.map((group) => group.id)
    assert.notInclude(groupIds, group.id)
  })

  test('should attach a teacher', async (assert) => {
    const lesson = await LessonFactory.create()
    const teacher = await TeacherFactory.create()

    await supertest(baseUrl)
      .post(`/lessons/${lesson.id}/teachers/${teacher.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('teachers')
    const teacherIds = lesson.teachers.map((student) => student.id)
    assert.include(teacherIds, teacher.id)
  })

  test('should detach a teacher', async (assert) => {
    const lesson = await LessonFactory.create()
    const teacher = await TeacherFactory.create()
    await lesson.related('teachers').save(teacher)

    await supertest(baseUrl)
      .delete(`/lessons/${lesson.id}/teachers/${teacher.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('teachers')
    const teacherIds = lesson.teachers.map((student) => student.id)
    assert.notInclude(teacherIds, teacher.id)
  })

  test('should attach a class', async (assert) => {
    const lesson = await LessonFactory.create()
    const theClass = await ClassFactory.create()

    await supertest(baseUrl)
      .post(`/lessons/${lesson.id}/classes/${theClass.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('classes')
    const classIds = lesson.classes.map((theClass) => theClass.id)
    assert.include(classIds, theClass.id)
  })

  test('should detach a class', async (assert) => {
    const lesson = await LessonFactory.create()
    const theClass = await ClassFactory.create()
    await lesson.related('classes').save(theClass)

    await supertest(baseUrl)
      .delete(`/lessons/${lesson.id}/classes/${theClass.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('classes')
    const classIds = lesson.classes.map((theClass) => theClass.id)
    assert.notInclude(classIds, theClass.id)
  })

  test('should attach a subject', async (assert) => {
    const lesson = await LessonFactory.create()
    const subject = await SubjectFactory.create()

    await supertest(baseUrl)
      .post(`/lessons/${lesson.id}/subjects/${subject.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('subjects')
    const subjectIds = lesson.subjects.map((subject) => subject.id)
    assert.include(subjectIds, subject.id)
  })

  test('should detach a subject', async (assert) => {
    const lesson = await LessonFactory.create()
    const subject = await SubjectFactory.create()
    await lesson.related('subjects').save(subject)

    await supertest(baseUrl)
      .delete(`/lessons/${lesson.id}/subjects/${subject.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('subjects')
    const subjectIds = lesson.subjects.map((subject) => subject.id)
    assert.notInclude(subjectIds, subject.id)
  })

  test('should attach a classroom', async (assert) => {
    const lesson = await LessonFactory.create()
    const classroom = await ClassroomFactory.create()

    await supertest(baseUrl)
      .post(`/lessons/${lesson.id}/classrooms/${classroom.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('classrooms')
    const classroomIds = lesson.classrooms.map((classroom) => classroom.id)
    assert.include(classroomIds, classroom.id)
  })

  test('should detach a classroom', async (assert) => {
    const lesson = await LessonFactory.create()
    const classroom = await ClassroomFactory.create()
    await lesson.related('classrooms').save(classroom)

    await supertest(baseUrl)
      .delete(`/lessons/${lesson.id}/classrooms/${classroom.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await lesson.refresh()
    await lesson.load('classrooms')
    const classroomIds = lesson.classrooms.map((classroom) => classroom.id)
    assert.notInclude(classroomIds, classroom.id)
  })
})
