import test from 'japa'
import supertest from 'supertest'

import { baseUrl, createFakeAdmin } from '../test-helpers'

import Admin from 'App/Models/Admin'

test.group('Init sequence', () => {
  test('should authorize init if there is no admin', async (assert) => {
    const admins = await Admin.all()
    await Promise.all(admins.map((admin) => admin.delete()))
    const { body } = await supertest(baseUrl)
      .get('/init')
      .expect('Content-Type', /json/)
      .expect(200)

    assert.isTrue(body.can_init)
  })

  test('should not create an admin if there is admins', async (assert) => {
    await createFakeAdmin()
    const { body } = await supertest(baseUrl)
      .get('/init')
      .expect('Content-Type', /json/)
      .expect(200)

    assert.isFalse(body.can_init)
  })

  test('should create an admin if there is no admin', async (assert) => {
    const admins = await Admin.all()
    await Promise.all(admins.map((admin) => admin.delete()))
    const payload = { username: 'test', password: 'test', name: 'test' }
    const { body } = await supertest(baseUrl)
      .post('/init')
      .send(payload)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(payload.name, body.name)
  })

  test('should not create an admin if there is admins', async () => {
    await createFakeAdmin()
    const payload = { username: 'test', password: 'test', name: 'test' }
    await supertest(baseUrl).post('/init').send(payload).expect('Content-Type', /json/).expect(401)
  })
})
