import test from 'japa'
import supertest from 'supertest'
import { baseUrl, loginAndGetToken, logout, paginationKeys } from '../test-helpers'

import Subject from 'App/Models/Subject'
import { SubjectFactory } from 'Database/factories'

let token

test.group('Subject', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all subjects', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/subjects')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)
    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a subject', async (assert) => {
    const { id } = await SubjectFactory.create()
    const { body } = await supertest(baseUrl)
      .get(`/subjects/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the subject does not exists', async () => {
    const subject = await Subject.find(1)
    if (subject) await subject.delete()
    await supertest(baseUrl)
      .get('/subjects/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a subject', async (assert) => {
    const data = { title: 'efzef', description: 'mmmm' }
    const { body } = await supertest(baseUrl)
      .post('/subjects')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdSubject = await Subject.find(body.id)
    assert.isNotNull(createdSubject)
    assert.equal(createdSubject?.title, data.title)
    assert.equal(createdSubject?.description, data.description)
  })

  test('should update a subject', async (assert) => {
    const subject = await SubjectFactory.create()

    const data = { title: 'example', description: 'Test' }
    await supertest(baseUrl)
      .patch(`/subjects/${subject.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await subject.refresh()

    assert.equal(subject.title, data.title)
    assert.equal(subject.description, data.description)
  })

  test('should delete a subject', async (assert) => {
    const subject = await SubjectFactory.create()

    await supertest(baseUrl)
      .delete(`/subjects/${subject.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedSubject = await Subject.find(subject.id)
    assert.isNull(deletedSubject)
  })
})
