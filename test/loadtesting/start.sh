vegeta attack  \
  -header "Authorization: Bearer $TOKEN" \
  -duration=15s \
  -rate=20 \
  -name=20qps \
  -targets targets.txt \
  -output results.20qps.bin

vegeta attack  \
  -header "Authorization: Bearer $TOKEN" \
  -duration=15s \
  -rate=100 \
  -name=100qps \
  -targets targets.txt \
  -output results.100qps.bin

vegeta attack  \
  -header "Authorization: Bearer $TOKEN" \
  -duration=15s \
  -rate=200 \
  -name=200qps \
  -targets targets.txt \
  -output results.200qps.bin

vegeta attack  \
  -header "Authorization: Bearer $TOKEN" \
  -duration=15s \
  -rate=500 \
  -name=500qps \
  -targets targets.txt \
  -output results.500qps.bin

vegeta plot -output plot.html results.*
