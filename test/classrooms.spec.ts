import test from 'japa'
import supertest from 'supertest'
import { baseUrl, loginAndGetToken, logout, paginationKeys } from '../test-helpers'

import Classroom from 'App/Models/Classroom'
import { ClassroomFactory } from 'Database/factories'

let token

test.group('Classroom', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all classrooms', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/classrooms')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a classroom', async (assert) => {
    const { id } = await ClassroomFactory.create()
    const { body } = await supertest(baseUrl)
      .get(`/classrooms/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the classroom does not exists', async () => {
    const classroom = await Classroom.find(1)
    if (classroom) await classroom.delete()
    await supertest(baseUrl)
      .get('/classrooms/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a classroom', async (assert) => {
    const data = { title: 'efzef', description: 'mmmm' }
    const { body } = await supertest(baseUrl)
      .post('/classrooms')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdClassroom = await Classroom.find(body.id)
    assert.isNotNull(createdClassroom)
    assert.equal(createdClassroom?.title, data.title)
    assert.equal(createdClassroom?.description, data.description)
  })

  test('should update a classroom', async (assert) => {
    const classroom = await ClassroomFactory.create()

    const data = { title: 'example', description: 'Test' }
    await supertest(baseUrl)
      .patch(`/classrooms/${classroom.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await classroom.refresh()

    assert.equal(classroom.title, data.title)
    assert.equal(classroom.description, data.description)
  })

  test('should delete a classroom', async (assert) => {
    const classroom = await ClassroomFactory.create()

    await supertest(baseUrl)
      .delete(`/classrooms/${classroom.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedClassroom = await Classroom.find(classroom.id)
    assert.isNull(deletedClassroom)
  })
})
