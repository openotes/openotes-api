import test from 'japa'
import supertest from 'supertest'
import {
  baseUrl,
  createFakeStudent,
  loginAndGetToken,
  logout,
  paginationKeys,
} from '../test-helpers'
import { DateTime } from 'luxon'

import Student from 'App/Models/Student'
import { StudentFactory } from 'Database/factories'

let token

test.group('Student', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all students', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/students')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)
    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a student', async (assert) => {
    const { id } = await StudentFactory.create()
    const { body } = await supertest(baseUrl)
      .get(`/students/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the student does not exists', async () => {
    const student = await Student.find(1)
    if (student) await student.delete()

    await supertest(baseUrl)
      .get('/students/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a student', async (assert) => {
    const data = {
      username: 'example',
      password: 'esfezf',
      firstname: 'Fez',
      lastname: 'Foo',
      birthDate: DateTime.local().toISODate(),
    }
    const { body } = await supertest(baseUrl)
      .post('/students')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdStudent = await Student.find(body.id)
    assert.isNotNull(createdStudent)
    await createdStudent?.load('user')
    assert.equal(createdStudent?.user.username, data.username)
  })

  test('should update a student', async (assert) => {
    const student = await createFakeStudent()

    const data = {
      username: 'example',
      firstname: 'New',
      lastname: 'AlsoNew',
      birthDate: DateTime.local(),
    }
    await supertest(baseUrl)
      .patch(`/students/${student.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await student.refresh()
    await student.load('user')

    assert.equal(student.firstname, data.firstname)
    assert.equal(student.lastname, data.lastname)
    assert.equal(student.user.username, data.username)
  })

  test('should delete a student', async (assert) => {
    const student = await createFakeStudent()

    await supertest(baseUrl)
      .delete(`/students/${student.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedStudent = await Student.find(student.id)
    assert.isNull(deletedStudent)
  })
})
