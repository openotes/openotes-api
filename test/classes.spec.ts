import test from 'japa'
import supertest from 'supertest'
import {
  baseUrl,
  createFakeStudent,
  createFakeTeacher,
  loginAndGetToken,
  logout,
  paginationKeys,
} from '../test-helpers'

import Class from 'App/Models/Class'
import { ClassFactory } from 'Database/factories'

let token

test.group('Class', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all classes', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/classes')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a class', async (assert) => {
    const { id } = await ClassFactory.create()

    const { body } = await supertest(baseUrl)
      .get(`/classes/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the class does not exists', async () => {
    const theClass = await Class.find(1)
    if (theClass) await theClass.delete()

    await supertest(baseUrl)
      .get('/classes/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a class', async (assert) => {
    const data = { title: 'efzef', description: 'mmmm' }

    const { body } = await supertest(baseUrl)
      .post('/classes')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdClass = await Class.find(body.id)
    assert.isNotNull(createdClass)
    assert.equal(createdClass?.title, data.title)
    assert.equal(createdClass?.description, data.description)
  })

  test('should update a class', async (assert) => {
    const theClass = await ClassFactory.create()

    const data = { title: 'example', description: 'Test' }
    await supertest(baseUrl)
      .patch(`/classes/${theClass.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await theClass.refresh()

    assert.equal(theClass.title, data.title)
    assert.equal(theClass.description, data.description)
  })

  test('should delete a class', async (assert) => {
    const { id: classId } = await ClassFactory.create()

    await supertest(baseUrl)
      .delete(`/classes/${classId}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedClass = await Class.find(classId)
    assert.isNull(deletedClass)
  })

  test('should attach student', async (assert) => {
    const theClass = await ClassFactory.create()

    const { id: studentId } = await createFakeStudent()

    await supertest(baseUrl)
      .post(`/classes/${theClass.id}/students/${studentId}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await theClass.refresh()
    await theClass.load('students')

    const studentIds = theClass.students.map((student) => student.id)
    assert.include(studentIds, studentId)
  })

  test('should detach student', async (assert) => {
    const theClass = await ClassFactory.create()
    const student = await createFakeStudent()

    await theClass.related('students').save(student)

    await supertest(baseUrl)
      .delete(`/classes/${theClass.id}/students/${student.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await theClass.refresh()
    await theClass.load('students')

    const studentIds = theClass.students.map((student) => student.id)
    assert.notInclude(studentIds, student.id)
  })

  test('should attach head teacher', async (assert) => {
    const theClass = await ClassFactory.create()

    const { id: teacherId } = await createFakeTeacher()

    await supertest(baseUrl)
      .post(`/classes/${theClass.id}/teachers/${teacherId}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await theClass.refresh()
    await theClass.load('headTeachers')

    const teacherIds = theClass.headTeachers.map((student) => student.id)
    assert.include(teacherIds, teacherId)
  })

  test('should detach head teacher', async (assert) => {
    const theClass = await ClassFactory.create()
    const teacher = await createFakeTeacher()
    await theClass.related('headTeachers').save(teacher)

    await supertest(baseUrl)
      .delete(`/classes/${theClass.id}/teachers/${teacher.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await theClass.refresh()
    await theClass.load('headTeachers')

    const teacherIds = theClass.headTeachers.map((student) => student.id)
    assert.notInclude(teacherIds, teacher.id)
  })
})
