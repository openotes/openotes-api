import test from 'japa'
import supertest from 'supertest'
import { baseUrl } from '../test-helpers'

import User from 'App/Models/User'

test.group('Login', () => {
  test('should login if credentials are correct', async (assert) => {
    const credentials = { username: 'ez', password: 'test', role: 'admin' as const }
    await User.create(credentials)

    const { body } = await supertest(baseUrl)
      .post('/login')
      .send(credentials)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.property(body, 'token')
  })
  test('should return an error if credentials are not correct', async () => {
    await supertest(baseUrl)
      .post('/login')
      .send({ username: 'no-exists', password: 'test' })
      .expect('Content-Type', /json/)
      .expect(400)
  })
})
