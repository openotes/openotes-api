import test from 'japa'
import supertest from 'supertest'
import {
  baseUrl,
  createFakeStudent,
  loginAndGetToken,
  logout,
  paginationKeys,
} from '../test-helpers'

import Group from 'App/Models/Group'
import { GroupFactory, StudentFactory } from 'Database/factories'

let token

test.group('Group', (group) => {
  group.before(async () => {
    token = await loginAndGetToken()
  })

  group.after(async () => {
    await logout(token)
  })

  test('should list all groups', async (assert) => {
    const { body } = await supertest(baseUrl)
      .get('/groups')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.hasAllKeys(body, paginationKeys)
  })

  test('should get a group', async (assert) => {
    const { id } = await GroupFactory.create()
    const { body } = await supertest(baseUrl)
      .get(`/groups/${id}`)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal(body.id, id)
  })

  test('should return 404 if the group does not exists', async () => {
    const group = await Group.find(1)
    if (group) await group.delete()
    await supertest(baseUrl)
      .get('/groups/1')
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should create a group', async (assert) => {
    const data = { title: 'efzef', description: 'mmmm' }
    const { body } = await supertest(baseUrl)
      .post('/groups')
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(201)

    const createdGroup = await Group.find(body.id)
    assert.isNotNull(createdGroup)
    assert.equal(createdGroup?.title, data.title)
    assert.equal(createdGroup?.description, data.description)
  })

  test('should update a group', async (assert) => {
    const group = await GroupFactory.create()

    const data = { title: 'example', description: 'Test' }
    await supertest(baseUrl)
      .patch(`/groups/${group.id}`)
      .send(data)
      .auth(token, { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(200)

    await group.refresh()

    assert.equal(group.title, data.title)
    assert.equal(group.description, data.description)
  })

  test('should delete a group', async (assert) => {
    const group = await GroupFactory.create()

    await supertest(baseUrl)
      .delete(`/groups/${group.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    const deletedGroup = await Group.find(group.id)
    assert.isNull(deletedGroup)
  })

  test('should attach student', async (assert) => {
    const group = await GroupFactory.create()
    const student = await createFakeStudent()

    await supertest(baseUrl)
      .post(`/groups/${group.id}/students/${student.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await group.refresh()
    await group.load('students')

    const studentIds = group.students.map((student) => student.id)
    assert.include(studentIds, student.id)
  })

  test('should detach student', async (assert) => {
    const group = await GroupFactory.create()
    const student = await StudentFactory.create()
    await group.related('students').save(student)

    await supertest(baseUrl)
      .delete(`/groups/${group.id}/students/${student.id}`)
      .auth(token, { type: 'bearer' })
      .expect(204)

    await group.refresh()
    await group.load('students')

    const studentIds = group.students.map((student) => student.id)
    assert.notInclude(studentIds, student.id)
  })
})
