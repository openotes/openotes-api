import test from 'japa'
import supertest from 'supertest'

import { baseUrl } from '../test-helpers'

test.group('Auth', () => {
  test('should unauthorize if token is invalid', async () => {
    await supertest(baseUrl)
      .get(`/students`)
      .auth('wrongtoken', { type: 'bearer' })
      .expect('Content-Type', /json/)
      .expect(401)
  })
})
