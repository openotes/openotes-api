import 'reflect-metadata'
import { join } from 'path'
import execa from 'execa'
import { configure } from 'japa'
import sourceMapSupport from 'source-map-support'

process.env.NODE_ENV = 'testing'
process.env.HOST = '127.0.0.1'
process.env.LOG_LEVEL = 'error'
process.env.APP_NAME = 'openotes'
process.env.PORT = '3333'
process.env.APP_KEY = '494c3927ee37fc3dcea4370097be10837f944235'
process.env.ADONIS_ACE_CWD = join(__dirname)
sourceMapSupport.install({ handleUncaughtExceptions: false })

async function runMigrations() {
  console.log('Running migrations...')
  await execa.node('ace', ['migration:run'])
}

async function rollbackMigrations() {
  console.log('Rollbacking migrations...')
  await execa.node('ace', ['migration:rollback'])
}

async function startHttpServer() {
  const { Ignitor } = await import('@adonisjs/core/build/src/Ignitor')
  await new Ignitor(__dirname).httpServer().start()
}

/**
 * Configure test runner
 */
configure({
  files: ['test/**/*.spec.ts'],
  before: [startHttpServer, runMigrations],
  after: [rollbackMigrations],
})
